let trainer = {
    name: "Ash Ketchum",
    age: 10,
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: ["Picachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function() {
        console.log(this.pokemon[0] + "! I choose you!")
    }
}
console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name)
console.log("Result of square bracket notation: ");
console.log(trainer.pokemon);
console.log("Result of talk method")
trainer.talk();